# Use an official Python runtime as a parent image
FROM python:3.7-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in Pipenv
RUN pip install pipenv
RUN pipenv install

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME Pranas

# Run app.py when the container launches
CMD ["pipenv", "run", "./app.py"]
