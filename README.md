# Hello Docker

Docker demo project, based on https://docs.docker.com/get-started.


## Container

### Usage
```
docker build -t hellodocker .
docker image ls
docker run -p 4000:80 hellodocker
curl http://localhost:4000
```

### Cheat sheet
```
# List Docker CLI commands
docker
docker container --help

# Display Docker version and info
docker --version
docker version
docker info

# Execute Docker image
docker run hello-world

# List Docker images
docker image ls

# List Docker containers (running, all, all in quiet mode)
docker container ls
docker container ls --all
docker container ls -aq
```

## Registry

### Usage
```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/pziaukas-repos/hello/hellodocker .
docker push registry.gitlab.com/pziaukas-repos/hello/hellodocker
```

## Swarm

### Usage
If you'd like, add Redis persistence location in `docker-compose.yml`, then
```
docker swarm init
docker stack deploy -c docker-compose.yml hellodockerswarm
docker service ls
docker service ps hellodockerswarm_web
curl http://localhost:4000

docker stack rm hellodockerswarm
docker swarm leave --force
```

### Cheat sheet
```
# List stacks or apps
docker stack ls

# Run the specified Compose file
docker stack deploy -c <composefile> <appname>

# List running services associated with an app
docker service ls

# List tasks associated with an app
docker service ps <service>

# Inspect task or container
docker inspect <task or container>

# List container IDs
docker container ls -q

# Tear down an application
docker stack rm <appname>

# Take down a single node swarm from the manager
docker swarm leave --force
```
